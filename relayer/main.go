package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	golog "github.com/ipfs/go-log"

	"github.com/libp2p/go-libp2p"
	circuit "github.com/libp2p/go-libp2p-circuit"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/network"
	"github.com/libp2p/go-libp2p-core/peer"
	libp2pquic "github.com/libp2p/go-libp2p-quic-transport"
	secio "github.com/libp2p/go-libp2p-secio"
	libp2ptls "github.com/libp2p/go-libp2p-tls"
	"github.com/multiformats/go-multiaddr"

	ma "github.com/multiformats/go-multiaddr"
)

func makeMultiTransportHost(port int, secure bool, beRelay bool) (host.Host, error) {
	priv, _, _ := crypto.GenerateKeyPair(crypto.Secp256k1, 256)

	options := []libp2p.Option{
		libp2p.Identity(priv),

		libp2p.Transport(libp2pquic.NewTransport),
		libp2p.DefaultTransports,
	}

	if secure {
		options = append(
			options,
			libp2p.Security(libp2ptls.ID, libp2ptls.New),
			libp2p.Security(secio.ID, secio.New),
		)
	} else {
		options = append(
			options,
			libp2p.NoSecurity,
		)
	}

	if beRelay {
		options = append(
			options,
			libp2p.EnableRelay(circuit.OptHop),
			libp2p.ListenAddrStrings(
				fmt.Sprintf("/ip4/0.0.0.0/tcp/%d", port),
				fmt.Sprintf("/ip4/0.0.0.0/udp/%d/quic", port),
			),
		)
	} else {
		options = append(options, libp2p.ListenAddrs())
	}

	host, _ := libp2p.New(
		context.Background(),
		options...,
	)

	return host, nil
}

func decodeAddrStr(target string) (peer.ID, multiaddr.Multiaddr) {
	ipfsaddr, err := multiaddr.NewMultiaddr(target)
	if err != nil {
		log.Fatalln(err)
	}

	pid, err := ipfsaddr.ValueForProtocol(multiaddr.P_IPFS)
	if err != nil {
		log.Fatalln(err)
	}
	peerid, err := peer.IDB58Decode(pid)
	if err != nil {
		log.Fatalln(err)
	}

	targetPeerAddr, _ := multiaddr.NewMultiaddr(fmt.Sprintf("/ipfs/%s", pid))
	targetAddr := ipfsaddr.Decapsulate(targetPeerAddr)

	return peerid, targetAddr
}

func connectThroughRelay(relayAddr, targetAddr string, host host.Host) error {
	concat := relayAddr + "/p2p-circuit/p2p/" + targetAddr
	targetMA, err := ma.NewMultiaddr(concat)
	if err != nil {
		return err
	}
	targetID, err := peer.IDB58Decode(targetAddr)
	if err != nil {
		return err
	}

	targetInfo := peer.AddrInfo{
		ID:    targetID,
		Addrs: []multiaddr.Multiaddr{targetMA},
	}

	if err := host.Connect(context.Background(), targetInfo); err != nil {
		return err
	}

	s, err := host.NewStream(context.Background(), targetID, "/get-message")
	if err != nil {
		return err
	}

	s.Read(make([]byte, 1))

	return nil
}

func main() {
	golog.SetAllLoggers(golog.LevelError)

	port := flag.Int("port", 10000, "port")
	secure := flag.Bool("secure", false, "transport security")
	relayAddr := flag.String("relayAddr", "", "address of circuit relay")
	targetID := flag.String("targetID", "", "target peer id")

	flag.Parse()

	beRelay := *relayAddr == ""

	host, err := makeMultiTransportHost(*port, *secure, beRelay)
	if err != nil {
		panic(err)
	}

	fmt.Printf("My PeerID is: %s\n", host.ID())

	if beRelay {
		peerInfo := peer.AddrInfo{
			ID:    host.ID(),
			Addrs: host.Addrs(),
		}
		addrs, _ := peer.AddrInfoToP2pAddrs(&peerInfo)

		fmt.Println("You canreach this node using these addresses:")
		for _, addr := range addrs {
			fmt.Println(addr)
		}

		select {}
	}

	host.SetStreamHandler("/get-message", func(s network.Stream) {
		fmt.Printf("Received connection from %s\n", s.Conn().RemotePeer().Pretty())
		s.Close()
	})

	relayID, relayMA := decodeAddrStr(*relayAddr)

	// connect to relay
	if err := host.Connect(
		context.Background(),
		peer.AddrInfo{
			ID:    relayID,
			Addrs: []multiaddr.Multiaddr{relayMA},
		},
	); err != nil {
		log.Fatalf("Failed to connect to relay: %v\n", err)
	}

	// wait for incoming connections
	if *targetID == "" {
		fmt.Println("Waiting for connections")
		select {}
	}

	if err := connectThroughRelay(*relayAddr, *targetID, host); err != nil {
		log.Fatalf("Failed to dial target through relay: %v\n", err)
	}
}
