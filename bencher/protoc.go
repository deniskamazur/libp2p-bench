package main

import (
	"context"
	"io/ioutil"
	"log"

	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/network"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/libp2p/go-libp2p-core/peerstore"
	"github.com/multiformats/go-multiaddr"
)

const (
	TRANSFER_REQ = "/transfer/0.0.1"
)

type TransferHandler struct {
	host.Host
}

func NewTransferHandler(host host.Host) *TransferHandler {
	th := TransferHandler{Host: host}
	th.Host.SetStreamHandler(TRANSFER_REQ, th.onRequest)

	return &th
}

func (th *TransferHandler) onRequest(s network.Stream) {
	bytes, err := ioutil.ReadAll(s)
	if err != nil {
		log.Println(err)
		return
	}
	log.Printf("received %d bytes\n", len(bytes))

	s.Write([]byte("ack"))
	s.Close()
}

func (th *TransferHandler) Transfer(data []byte, peerid peer.ID, addr multiaddr.Multiaddr) error {
	th.Host.Peerstore().AddAddr(peerid, addr, peerstore.PermanentAddrTTL)

	s, err := th.Host.NewStream(context.Background(), peerid, TRANSFER_REQ)
	if err != nil {
		return err
	}

	s.Write(data)
	s.CloseWrite()

	bytes, err := ioutil.ReadAll(s)
	if err != nil {
		return err
	}
	s.Close()

	_ = bytes

	// fmt.Printf("received response: %s\n", string(bytes))

	return nil
}
