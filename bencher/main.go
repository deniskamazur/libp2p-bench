package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"time"

	"github.com/libp2p/go-libp2p"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/peer"
	libp2pquic "github.com/libp2p/go-libp2p-quic-transport"
	secio "github.com/libp2p/go-libp2p-secio"
	libp2ptls "github.com/libp2p/go-libp2p-tls"
	"github.com/multiformats/go-multiaddr"

	golog "github.com/ipfs/go-log"
)

func makeMultiTransportHost(port int, secure bool) (host.Host, error) {
	priv, _, _ := crypto.GenerateKeyPair(crypto.Secp256k1, 256)

	options := []libp2p.Option{
		libp2p.Identity(priv),
		libp2p.ListenAddrStrings(
			fmt.Sprintf("/ip4/0.0.0.0/tcp/%d", port),
			fmt.Sprintf("/ip4/0.0.0.0/udp/%d/quic", port),
		),
		libp2p.Transport(libp2pquic.NewTransport),
		libp2p.DefaultTransports,
	}

	if secure {
		options = append(
			options,
			libp2p.Security(libp2ptls.ID, libp2ptls.New),
			libp2p.Security(secio.ID, secio.New),
		)
	} else {
		options = append(
			options,
			libp2p.NoSecurity,
		)
	}

	host, _ := libp2p.New(
		context.Background(),
		options...,
	)

	return host, nil
}

func decodeAddrStr(target string) (peer.ID, multiaddr.Multiaddr) {
	ipfsaddr, err := multiaddr.NewMultiaddr(target)
	if err != nil {
		log.Fatalln(err)
	}

	pid, err := ipfsaddr.ValueForProtocol(multiaddr.P_IPFS)
	if err != nil {
		log.Fatalln(err)
	}
	peerid, err := peer.IDB58Decode(pid)
	if err != nil {
		log.Fatalln(err)
	}

	targetPeerAddr, _ := multiaddr.NewMultiaddr(fmt.Sprintf("/ipfs/%s", pid))
	targetAddr := ipfsaddr.Decapsulate(targetPeerAddr)

	return peerid, targetAddr
}

func main() {
	golog.SetAllLoggers(golog.LevelError)

	port := flag.Int("p", 10000, "port")
	target := flag.String("t", "", "target address")
	secure := flag.Bool("s", false, "transport security")
	nbytes := flag.Int("nbytes", 104857600, "number of bytes to be sent to target")
	ntries := flag.Int("ntries", 5, "number of times to send package")
	flag.Parse()

	host, err := makeMultiTransportHost(*port, *secure)
	if err != nil {
		log.Fatalln(err)
	}

	node := NewTransferHandler(host)

	if *target == "" {
		peerInfo := peer.AddrInfo{
			ID:    node.ID(),
			Addrs: node.Addrs(),
		}
		addrs, _ := peer.AddrInfoToP2pAddrs(&peerInfo)
		fmt.Printf("Run './test -p %d -t %s'\n", *port+1, addrs[0])

		fmt.Println("You can also reach this node using other addresses:")
		for _, addr := range addrs {
			fmt.Println(addr)
		}

		select {}
	}

	peerid, targetAddr := decodeAddrStr(*target)
	bytes := make([]byte, *nbytes)

	start := time.Now()
	for i := 0; i < *ntries; i++ {
		fmt.Printf("Sending %d bytes to target: ", *nbytes)

		start := time.Now()
		if err := node.Transfer(bytes, peerid, targetAddr); err != nil {
			log.Fatalln(err)
		}

		elapsed := time.Since(start)
		fmt.Printf("took %s\n", elapsed)
	}
	elapsed := time.Since(start)
	fmt.Printf("\n%d packages took %s, average time: %s\n", *ntries, elapsed, time.Duration(int(elapsed) / *ntries))
}
