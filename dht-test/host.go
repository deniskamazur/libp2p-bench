package main

import (
	"context"

	"github.com/libp2p/go-libp2p"
	relay "github.com/libp2p/go-libp2p-circuit"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/network"
	"github.com/libp2p/go-libp2p-core/routing"
	dht "github.com/libp2p/go-libp2p-kad-dht"
	libp2pquic "github.com/libp2p/go-libp2p-quic-transport"
	secio "github.com/libp2p/go-libp2p-secio"
	libp2ptls "github.com/libp2p/go-libp2p-tls"

	ds "github.com/ipfs/go-datastore"
	dsync "github.com/ipfs/go-datastore/sync"
)

const (
	RelayHop = iota
	RelayClient
	RelayAuto
)

const protoName = "/noop/1.0.0"

func makeRoutedHost(public bool, relayOpt int) (host.Host, error) {
	priv, _, err := crypto.GenerateKeyPair(
		crypto.Ed25519,
		-1,
	)
	if err != nil {
		return nil, err
	}

	// default settings
	options := []libp2p.Option{
		libp2p.Identity(priv),
		libp2p.Security(libp2ptls.ID, libp2ptls.New),
		libp2p.Security(secio.ID, secio.New),
		libp2p.Routing(
			func(h host.Host) (routing.PeerRouting, error) {
				dstore := dsync.MutexWrap(ds.NewMapDatastore())
				idht := dht.NewDHT(context.Background(), h, dstore)
				return idht, err
			},
		),
	}

	switch relayOpt {
	case RelayAuto:
		options = append(options, libp2p.EnableAutoRelay())
	case RelayClient:
		options = append(options, libp2p.EnableRelay())
	case RelayHop:
		options = append(options, libp2p.EnableRelay(relay.OptHop))
	}

	// set up transort if node is public
	if public {
		options = append(
			options,
			libp2p.ListenAddrStrings(
				"/ip4/0.0.0.0/tcp/9000",
				"/ip4/0.0.0.0/udp/9000/quic",
			),
			libp2p.Transport(libp2pquic.NewTransport),
			libp2p.DefaultTransports,
			libp2p.ForceReachabilityPublic(),
		)
	} else {
		options = append(options, libp2p.ListenAddrs())
	}

	h, err := libp2p.New(
		context.Background(),
		options...,
	)

	if err != nil {
		return h, err
	}

	h.SetStreamHandler(protoName, func(s network.Stream) {
		s.Close()
	})

	return h, nil

	/*
		ctx := context.Background()

		dstore := dsync.MutexWrap(ds.NewMapDatastore())
		routedHost := rhost.Wrap(
			h,
			dht.NewDHT(ctx, h, dstore),
		)

		return routedHost, nil
	*/
}
