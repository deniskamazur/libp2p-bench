package main

import (
	"context"

	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/libp2p/go-libp2p-core/peerstore"
)

func main() {
	relay, err := makeRoutedHost(true, RelayAuto)
	if err != nil {
		panic(err)
	}

	h1, err := makeRoutedHost(false, RelayClient)
	if err != nil {
		panic(err)
	}

	h2, err := makeRoutedHost(false, RelayClient)
	if err != nil {
		panic(err)
	}

	if err := h1.Connect(
		context.Background(),
		peer.AddrInfo{
			ID:    relay.ID(),
			Addrs: relay.Addrs(),
		},
	); err != nil {
		panic(err)
	}

	if err := h2.Connect(
		context.Background(),
		peer.AddrInfo{
			ID:    relay.ID(),
			Addrs: relay.Addrs(),
		},
	); err != nil {
		panic(err)
	}

	h1.Peerstore().AddAddrs(relay.ID(), relay.Addrs(), peerstore.PermanentAddrTTL)
	if _, err := h1.NewStream(context.Background(), h2.ID(), protoName); err != nil {
		panic(err)
	}
}
